LFS_FAIL = $(shell head -n 1 "${CURDIR}/individual/lfs-test-file" | grep -E '^version')

default:
	@echo Please specify your targets!
	@exit 1

git:
	cp -Tabs "${CURDIR}/tabs/git" "${HOME}"
	git config --global core.excludesfile "${HOME}/.gitignore_global"
	git config --global core.editor vim
	git config --global push.default simple

bspwm: feh_random_bg
	cp -Tabs "${CURDIR}/tabs/bspwm" "${HOME}"

fonts:
	if [ -z "${LFS_FAIL}" ]; then \
		cp -Tafs "${CURDIR}/tabs/fonts" "${HOME}"; \
	fi
	CURDIR="${CURDIR}" "${CURDIR}/install-scripts/ubuntu-mono-nerd-font.sh"
	fc-cache -f

feh_random_bg:
	cp -Tabs "${CURDIR}/individual/feh_random_bg" "${HOME}/.local/bin/feh_random_bg"

# ensure pip for python2 & python3 is installed
# On Void Linux, also install:
# 	xbps-install libffi-devel readline-devel gdbm-devel libressl-devel
# 	expat-devel sqlite-devel bzip2-devel zlib-devel liblzma-devel
# On Debian/Ubuntu, install additional packages with:
# 	apt build-dep python python3
#
# this installs Node.js provider if npm is installed
neovim:
	cp -Tabs "${CURDIR}/tabs/neovim" "${HOME}"
	CURDIR="${CURDIR}" ${CURDIR}/install-scripts/neovim-providers.sh

fish:
	cp -Tabs "${CURDIR}/tabs/fish" "${HOME}"
	curl -Lo ~/.config/fish/functions/fisher.fish --create-dirs https://git.io/fisher
	fish -c 'fisher add jethrokuan/fzf oh-my-fish/theme-bobthefish laughedelic/pisces'
	fish -c 'set -U theme_powerline_fonts no'
	fish -c 'set -U theme_nerd_fonts no'

fish-fancy-fonts:
	fish -c 'set -U theme_powerline_fonts yes'
	fish -c 'set -U theme_nerd_fonts yes'

python-pip:
	CURDIR="${CURDIR}" "${CURDIR}/install-scripts/python-pip.sh" virtualenv numpy

cataclysm-dda:
	cp -Tabs "${CURDIR}/tabs/cataclysm-dda" "${HOME}"

xfce4:
	cp -Tabs "${CURDIR}/tabs/xfce4" "${HOME}"

anypaste:
	curl -Lo "${HOME}/.local/bin/anypaste" "https://anypaste.xyz/sh"
	chmod +x "${HOME}/.local/bin/anypaste"

clean:
	rm -rf "${CURDIR}/.cache"
