!
! Standard Colors
!=================

*background: #000000
*foreground: #ebece6
*color0:     #000000
*color1:     #fb4245
*color2:     #50fa7b
*color3:     #f0fa8b
*color4:     #49b9fe
*color5:     #fb4cb3
*color6:     #8be9fd
*color7:     #ededec
*color8:     #555555
*color9:     #fb4245
*color10:    #50fa7b
*color11:    #f0fa8b
*color12:    #49b9fe
*color13:    #fb4cb3
*color14:    #8be9fd
*color15:    #ededec

!
! Rofi Launcher
!===============

rofi.color-enabled:     true
rofi.color-window:      #282828, #282828, #268bd2
rofi.color-normal:      #282828, #ffffff, #282828, #268bd2, #ffffff
rofi.color-active:      #282828, #268bd2, #282828, #268bd2, #205171
rofi.color-urgent:      #282828, #f3843d, #282828, #268bd2, #ffc39c
rofi.font:              Courrier 13
rofi.modi:              drun,run,ssh
rofi.key-run:           SuperL+p
rofi.terminal:          urxvt
rofi.ssh-client:        ssh
rofi.run-shell-command: {terminal} -e '{cmd}'
rofi.ssh-command:       {terminal} -e "{ssh-client} {host}"

!
! Urxvt Config
!==============

URxvt.background:             [90]#101010
URxvt.scrollColor:            #d1d4e0
URxvt.highlightColor:         #252936
URxvt.highlightTextColor:     #C0C5CE
URxvt.cursorColor:            #e4e4e4
URxvt.depth:                  32
URxvt.perl-ext-common:        default,clipboard,matcher
URxvt.iso14755:               false
URxvt.keysym.Shift-Control-C: perl:clipboard:copy
URxvt.keysym.Shift-Control-V: perl:clipboard:paste
URxvt.clipboard.copycmd:      xsel -ib
URxvt.clipboard.pastecmd:     xsel -ob
URxvt.scrollBar:              false
URxvt.font:                   xft:monospace:size=11
URxvt.url-launcher:           firefox
URxvt.loginShell:             true
URxvt.fading:                 25
URxvt.internalBorder:         3
URxvt.externalBorder:         0
URxvt.secondaryScreen:        1
URxvt.secondaryScroll:        0
URxvt.matcher.button:         1
URxvt.transparent:            false

!
! Xft
!=====

Xft.dpi:       120
Xft.antialias: true
Xft.hinting:   true
Xft.rgba:      rgb
Xft.hintstyle: hintslight
