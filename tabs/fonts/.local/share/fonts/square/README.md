# Square

![](http://strlen.com/img/engine/sq/square_sample_8.png)

[Square](http://strlen.com/square/) is a TTF font intended for roguelike games,
created by Wouter van Oortmerssen. It is licensed under the [Creative Commons
Attribution 3.0 Unported
license](https://creativecommons.org/licenses/by/3.0/deed.en_US).
