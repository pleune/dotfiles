#! /bin/bash

set -e

python2 -m pip install --user --upgrade neovim
python3 -m pip install --user --upgrade setuptools neovim python-language-server[all]

# Only install Node.js provider if npm is installed
if command -v npm; then
	export NODE_PATH="$HOME/.local/share/nvim/nodejs/node_modules"
	mkdir -p "$NODE_PATH"
	cd "$NODE_PATH/.."
	npm install neovim javascript-typescript-langserver
fi

