#!/bin/sh
set -e

mkdir -p "${CURDIR}/.cache/"
SCRIPT="${CURDIR}/.cache/get-pip.py"
export PIP_USER="yes"

if [ ! -f "${CURDIR}/.cache/get-pip.py" ]; then
    curl "https://bootstrap.pypa.io/get-pip.py" -o "${SCRIPT}"
fi

if command -v python; then
    python "${SCRIPT}"
    python -m pip install --upgrade --ignore-installed pip "$@"
fi
if command -v python3; then
    python3 "${SCRIPT}"
    python3 -m pip install --upgrade --ignore-installed pip "$@"
fi
