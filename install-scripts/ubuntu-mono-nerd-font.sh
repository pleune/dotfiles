#!/bin/sh
set -e

mkdir -p "${CURDIR}/.cache/"

if [ ! -f "${CURDIR}/.cache/UbuntuMono.zip" ]; then
    curl -s -L  https://api.github.com/repos/ryanoasis/nerd-fonts/releases/latest | \
	egrep -o '/ryanoasis/nerd-fonts/releases/download/v[0-9.]*/UbuntuMono\.zip' | \
	wget --base=http://github.com/ -i - -O "${CURDIR}/.cache/UbuntuMono.zip"
fi

mkdir -p "${HOME}/.local/share/fonts/Ubuntu Mono (Nerd)"
unzip -o -d "${HOME}/.local/share/fonts/Ubuntu Mono (Nerd)" "${CURDIR}/.cache/UbuntuMono.zip"
mkdir -p "${HOME}/.config/fontconfig/conf.d/"
cp -Tabs "${CURDIR}/individual/fontconfig-ubuntu-mono-nerd-font.conf" "${HOME}/.config/fontconfig/conf.d/10-ubuntu-mono-nerd-font.conf"

# This needs to be run to notice the new files
# fc-cache -f
